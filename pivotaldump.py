from dotenv import find_dotenv, load_dotenv
from pivotalclient import PivotalClient
import json
import os


# Constants initialization
load_dotenv(find_dotenv())
API_TOKEN = os.environ['PIVOTAL_TRACKER_API_TOKEN']
PROJECT_ID = os.environ['PIVOTAL_TRACKER_PROJECT_ID']

# Initialise client
client = PivotalClient(api_token=API_TOKEN, project_id=PROJECT_ID)


if __name__ == '__main__':
    raw_stories = client.get_stories_by_filter(pivotal_filter=None)

    for raw_story in raw_stories:
        story = {'id': raw_story.get('id'),
                 'title': raw_story.get('name', 'No Title'),
                 'description': raw_story.get('description',
                                              'No Description'),
                 'weight': int(raw_story.get('estimate', 0))}
        filename = 'story_{}.json'.format(story['id'])

        with open(filename, 'w') as storyfile:
            json.dump(story, storyfile, indent=4, sort_keys=True)
